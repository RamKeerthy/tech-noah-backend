import pymongo

from Controllers.GroupController import GroupController
from Controllers.SessionHandlerController import SessionHandlerController
from Controllers.SignupController import SignupController
from Helper.DateTimeHelper import DateTimeHelper
from Helper.MongoDBHelper import MongoDBHelper
from Helper.ProfanityCheckHelper import ProfanityCheckHelper


class ChatController:
    mongoClient = None
    database = None
    messagesTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.messagesTable = MongoDBHelper.tables["messages"]

    def sendMessage(self, request):
        message = None
        userID = request.form["userID"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        groupID = request.form["groupID"]
        messageContent = request.form["messageContent"]

        if ProfanityCheckHelper.isProfanity(messageContent):
            messageContent = ProfanityCheckHelper.censorProfanity(messageContent)

        sentAt = DateTimeHelper.getCurrentUTCDateTime()
        readAt = ""

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.messagesTable]

        messageDict = {
            "groupID": groupID,
            "senderID": userID,
            "sentAt": sentAt,
            "messageContent": messageContent,
            "readAt": readAt,
        }

        response = collection.insert_one(messageDict)
        message = {
            "messageID": str(response.inserted_id)
        }

        signupController = SignupController()
        userDetail = signupController.getUserInfoForUserID(userID, "firstName")

        messageDict["senderFirstName"] = userDetail
        del messageDict["_id"]

        from ChatEvent.Events import socketio
        socketio.emit('message', messageDict, room=groupID)

        return message

    def getHistoricMessageForGroupID(self, request):
        message = None
        userID = request.form["userID"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        groupID = request.form["groupID"]

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.messagesTable]

        result = collection.find({"groupID": groupID}).sort([("_id", pymongo.DESCENDING)]).limit(1000000)
        result = list(result)

        groupController = GroupController()
        members = groupController.getAllUsersForGroupID(groupID)

        membersDict = dict()

        for index, member in enumerate(members):
            membersDict[member["_id"]] = member["firstName"]

        for index, message in enumerate(result):
            result[index]["messageID"] = str(message["_id"])
            result[index]["firstName"] = membersDict[message["senderID"]]
            del result[index]["_id"]

        result = {
            "messages": result
        }

        return result
