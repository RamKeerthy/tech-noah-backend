from Helper.AESCipherHelper import AESCipher
from Helper.EmailDeliveryHelper import EmailDeliveryHelper
from Helper.IDGenerator import IDGenerator
from Helper.MongoDBHelper import MongoDBHelper


class ForgotPasswordController:
    mongoClient = None
    database = None
    usersTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.usersTable = MongoDBHelper.tables["users"]

    def forgotPassword(self, request):
        email = request.form["email"]

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        myQuery = {"email": email}
        requiredFields = {
            "firstName": 1,
            "lastName": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        firstName = str(result["firstName"])
        lastName = str(result["lastName"])

        # Generate activation code and send it via email
        activationCode = IDGenerator.randomWithNDigits(8)
        # Send the activation code to email
        emailDeliveryHelper = EmailDeliveryHelper()
        response = emailDeliveryHelper.sendForgotPasswordEmail(firstName, lastName, email, activationCode)

        # Update the activation code in users collection
        myQuery = {
            "email": email
        }

        updatedIsValid = {
            "$set": {
                "activationCode": activationCode,
                "password": ""
            }
        }

        collection.update_one(myQuery, updatedIsValid)
        message = {"message": "Reset Password mail sent successfully"}
        return message

    def resetPassword(self, request):
        email = request.form["email"]
        password = AESCipher.encryptRSA(request.form["password"])
        activationCode = request.form["activationCode"]

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        myQuery = {
            "email": email,
            "activationCode": int(activationCode)
        }

        requiredFields = {
            "email": 1
        }

        result = collection.find_one(myQuery, requiredFields)

        if result is None:
            return {"error": "Not a valid Password Reset Code"}

        # Update the activation code in users collection
        myQuery = {
            "email": email
        }

        updatedIsValid = {
            "$set": {
                "password": password
            }
        }

        collection.update_one(myQuery, updatedIsValid)
        message = {"message": "Password reset is successfully"}
        return message
