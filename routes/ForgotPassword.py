from flask import Blueprint, request

from Controllers.ForgotPasswordController import ForgotPasswordController

forgotPasswordBlueprint = Blueprint('forgotPassword', __name__, )


@forgotPasswordBlueprint.route('/')
def index():
    return 'forgot password working'


@forgotPasswordBlueprint.route('/forgotPassword', methods=['POST'])
def forgotPassword():
    if request.method == 'POST':
        message = None
        try:
            forgotPasswordController = ForgotPasswordController()
            message = forgotPasswordController.forgotPassword(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@forgotPasswordBlueprint.route('/resetPassword', methods=['POST'])
def resetPassword():
    if request.method == 'POST':
        message = None
        try:
            forgotPasswordController = ForgotPasswordController()
            message = forgotPasswordController.resetPassword(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message
