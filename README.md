# Tech Noah API
## Install appropriate python version (3.9 or greater)

## Installing necessary python packages using pip
 Execute the following commands to install the necessary packages in command prompt
```
pip install pymongo
pip install 'pymongo[srv]'
pip install flask_pymongo
pip install sendgrid
pip install python-dateutil --upgrade
pip install Flask-SocketIO
pip install eventlet
pip install pycryptodome
pip install zoomus
pip install pyzoom
pip install pytz
pip install icalendar
pip install pycryptodome
pip install better-profanity
```