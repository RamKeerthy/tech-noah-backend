import pymongo
from Startup import Startup


class MongoDBHelper:
    username = None
    password = None
    name = None
    tables = None

    mongoClient = None

    def __init__(self):
        config = Startup.getConfig()
        MongoDBHelper.username = config["database"]["username"]
        MongoDBHelper.password = config["database"]["password"]
        MongoDBHelper.name = config["database"]["name"]
        MongoDBHelper.tables = config["database"]["tables"]

        # Tech-Noah-Dev
        # connectionString = "mongodb+srv://{username}:{password}@cluster0.ariyg.mongodb.net/" \
        #                    "{database}?retryWrites=true&w=majority"

        # Tech-Noah-Prod
        connectionString = "mongodb+srv://{username}:{password}@prod.mmiwj.mongodb.net/myFirstDatabase?retryWrites" \
                           "=true&w=majority"

        # mongodb+srv://technoah:<password>

        connectionString = connectionString.format(
            username=MongoDBHelper.username,
            password=MongoDBHelper.password,
            database=MongoDBHelper.name
        )
        MongoDBHelper.mongoClient = pymongo.MongoClient(connectionString,
                                                        connectTimeoutMS=30000,
                                                        socketTimeoutMS=None,
                                                        socketKeepAlive=True,
                                                        connect=False,
                                                        maxPoolsize=1)

    @staticmethod
    def getMongoClient():
        if MongoDBHelper.mongoClient is None:
            MongoDBHelper()
        return MongoDBHelper.mongoClient

    @staticmethod
    def getDatabaseName():
        if MongoDBHelper.name is None:
            MongoDBHelper()
        return MongoDBHelper.name

    @staticmethod
    def getTables():
        if MongoDBHelper.tables is None:
            MongoDBHelper()
        return MongoDBHelper.tables
