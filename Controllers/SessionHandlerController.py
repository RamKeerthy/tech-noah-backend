from bson import ObjectId

from Helper.DateTimeHelper import DateTimeHelper
from Helper.IDGenerator import IDGenerator
from Helper.MongoDBHelper import MongoDBHelper


class SessionHandlerController:
    mongoClient = None
    database = None
    usersTable = None
    sessionTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.usersTable = MongoDBHelper.tables["users"]
        self.sessionTable = MongoDBHelper.tables["session"]

    def doesUserHaveValidSession(self, userID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.sessionTable]

        result = collection.find({"userID": userID})
        result = list(result)
        currentSession = result[0]

        lastActiveTime = currentSession["lastActiveTime"]

        isValidSession = {
            "isValidSession": DateTimeHelper.isTimeInGivenBound(lastActiveTime),
            "currentSession": currentSession["token"]
        }

        return isValidSession

    def createSessionID(self, userID):
        isValidSessionDict = self.doesUserHaveValidSession(userID)

        isValidSession = isValidSessionDict["isValidSession"]
        currentSession = isValidSessionDict["currentSession"]

        if isValidSession and (len(currentSession) > 0):
            # Return the existing session id
            return currentSession
        else:
            newSessionID = IDGenerator.generateNewSessionID()

            myQuery = {
                "userID": userID
            }

            updatedIsValid = {
                "$set": {
                    "token": newSessionID,
                    "lastActiveTime": DateTimeHelper.getCurrentUTCDateTime()
                }
            }

            databaseConnection = self.mongoClient[self.database]
            collection = databaseConnection[self.sessionTable]

            collection.update_one(myQuery, updatedIsValid)

            return newSessionID

    def invalidateSession(self, userID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.sessionTable]

        myQuery = {
            "userID": userID
        }

        updatedIsValid = {
            "$set": {
                "token": ""
            }
        }
        collection.update_one(myQuery, updatedIsValid)

        return True

    def createNewSessionID(self, userID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.sessionTable]

        sessionDict = {
            "userID": userID,
            "token": IDGenerator.generateNewSessionID(),
            "lastActiveTime": DateTimeHelper.getCurrentUTCDateTime()
        }

        response = collection.insert_one(sessionDict)
        return str(response.inserted_id)

    def updateLastActiveTime(self, userID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.sessionTable]

        myQuery = {
            "userID": userID
        }

        updatedIsValid = {
            "$set": {
                "lastActiveTime": DateTimeHelper.getCurrentUTCDateTime()
            }
        }

        collection.update_one(myQuery, updatedIsValid)

        return True


