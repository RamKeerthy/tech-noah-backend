from flask import Blueprint, request

from Controllers.ChatController import ChatController

chatBlueprint = Blueprint('chats', __name__, )


@chatBlueprint.route('/')
def index():
    return 'chats working'


@chatBlueprint.route('/sendMessage', methods=['POST'])
def sendMessage():
    if request.method == 'POST':
        message = None
        try:
            chatController = ChatController()
            message = chatController.sendMessage(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@chatBlueprint.route('/getHistoricMessageForGroupID', methods=['POST'])
def getHistoricMessageForGroupID():
    if request.method == 'POST':
        message = None
        try:
            chatController = ChatController()
            message = chatController.getHistoricMessageForGroupID(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message
