import re

import pymongo
from bson import ObjectId

from Controllers.SessionHandlerController import SessionHandlerController
from Controllers.SignupController import SignupController
from Controllers.ZoomController import ZoomController
from Helper.DateTimeHelper import DateTimeHelper
from Helper.EmailDeliveryHelper import EmailDeliveryHelper
from Helper.MongoDBHelper import MongoDBHelper
from Helper.ProfanityCheckHelper import ProfanityCheckHelper


class GroupController:
    mongoClient = None
    database = None
    groupsTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.groupsTable = MongoDBHelper.tables["groups"]

    def createGroup(self, request):
        message = None
        userID = request.form["userID"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        meetingTopic = request.form["meetingTopic"]
        meetingSubject = request.form["meetingSubject"]
        meetingTime = request.form["meetingTime"]
        meetingDuration = request.form["meetingDuration"]
        location = request.form["location"]

        if ProfanityCheckHelper.isProfanity(meetingTopic + " " + meetingDuration + " " + location):
            message = {
                "error": "Profanity detected change the fields and try creating the group"
            }
            return message

        timeCreated = DateTimeHelper.getCurrentUTCDateTime()
        members = userID

        # create zoom meeting
        zoomController = ZoomController()
        signupController = SignupController()

        hostEmail = signupController.getUserInfoForUserID(userID)
        hostFirstName = signupController.getUserInfoForUserID(userID, "firstName")
        hostLastName = signupController.getUserInfoForUserID(userID, "lastName")
        meetingDetails = zoomController.createZoomMeeting(hostEmail, meetingTopic, meetingTime, meetingDuration)

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        groupDict = {
            "meetingTopic": meetingTopic,
            "meetingSubject": meetingSubject,
            "meetingTime": meetingTime,
            "meetingDuration": meetingDuration,
            "timeCreated": timeCreated,
            "adminMember": userID,
            "members": members,
            "location": location,
            "zoomMeetingLink": meetingDetails.join_url
        }

        emailDeliveryHelper = EmailDeliveryHelper()
        emailDeliveryHelper.sendZoomMeetingLink(groupDict, hostEmail, hostFirstName, hostLastName)

        response = collection.insert_one(groupDict)
        message = {
            "groupID": str(response.inserted_id)
        }

        return message

    def updateMembers(self, request):
        message = None
        userID = request.form["userID"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        groupID = request.form["groupID"]
        addUserEmail = request.form["addUserEmail"]

        signupController = SignupController()
        addUserID = signupController.getUserIDForEmail(addUserEmail)
        addUserFirstName = signupController.getUserInfoForUserID(userID, "firstName")
        addUserLastName = signupController.getUserInfoForUserID(userID, "lastName")
        groupDetails = self.getGroupDetails(groupID)

        emailDeliveryHelper = EmailDeliveryHelper()
        emailDeliveryHelper.sendZoomMeetingLink(groupDetails, addUserEmail, addUserFirstName, addUserLastName)

        members = groupDetails["members"]

        myQuery = {
            "_id": ObjectId(groupID)
        }

        updateGroupMembers = {
            "$set": {
                "members": members + "|" + addUserID
            }
        }

        collection.update_one(myQuery, updateGroupMembers)

        message = {
            "success": "Member added to the group"
        }

        return message

    def getGroupDetails(self, groupID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        query = {
            "_id": ObjectId(groupID)
        }

        result = collection.find_one(query)

        return result

    def getAllGroupsForUser(self, request):
        signupController = SignupController()

        userID = request.form["userID"]
        searchKeyword = ""

        if "searchKeyword" in request.form:
            searchKeyword = request.form["searchKeyword"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        result = None

        if len(searchKeyword) > 0:
            result = collection.find({"meetingTopic": re.compile(searchKeyword, re.IGNORECASE)}).sort(
                [("meetingTime", pymongo.ASCENDING)])
        else:
            result = collection.find({}).sort([("meetingTime", pymongo.ASCENDING)])

        groups = list(result)
        filteredGroups = list()

        for index, group in enumerate(groups):
            groups[index]["groupID"] = str(group["_id"])
            del groups[index]["_id"]

            groupMembers = group["members"].split("|")

            if userID not in groupMembers:
                continue

            groupMembersList = signupController.getAllUsersForUserIDs(groupMembers)
            members = str()

            for memberIndex, member in enumerate(groupMembersList):
                if memberIndex > 0:
                    members = members + ", "

                members = members + member["firstName"]

            # groups[index]["members"] = members
            groups[index]["members"] = groupMembersList

            filteredGroups.append(groups[index])

        return filteredGroups

    def getAllGroups(self, request):
        signupController = SignupController()

        userID = request.form["userID"]
        searchKeyword = ""

        if "searchKeyword" in request.form:
            searchKeyword = request.form["searchKeyword"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        result = None

        if len(searchKeyword) > 0:
            result = collection.find({"meetingTopic": re.compile(searchKeyword, re.IGNORECASE)}).sort(
                [("meetingTime", pymongo.ASCENDING)])
        else:
            result = collection.find({}).sort([("meetingTime", pymongo.ASCENDING)])

        groups = list(result)
        filteredGroups = list()

        for index, group in enumerate(groups):
            groups[index]["groupID"] = str(group["_id"])
            del groups[index]["_id"]

            groupMembers = group["members"].split("|")

            if userID in groupMembers or not DateTimeHelper.isTimeInGivenBound(
                    groups[index]["meetingTime"], 0):
                continue

            groupMembersList = signupController.getAllUsersForUserIDs(groupMembers)
            members = str()

            for memberIndex, member in enumerate(groupMembersList):
                if memberIndex > 0:
                    members = members + ", "

                members = members + member["firstName"]

            # groups[index]["members"] = members
            groups[index]["members"] = groupMembersList

            filteredGroups.append(groups[index])

        return filteredGroups

    def getAllUsersForGroupID(self, groupID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        myQuery = {
            "_id": ObjectId(groupID)
        }

        requiredFields = {
            "_id": True,
            "members": 1
        }

        result = collection.find_one(myQuery, requiredFields)

        groupMembers = []

        if result:
            groupMembers = result["members"].split("|")

        signupController = SignupController()
        members = signupController.getAllUsersForUserIDs(groupMembers)

        return members

    def getGroupsForKeyword(self, request):
        signupController = SignupController()

        userID = request.form["userID"]
        searchKeyword = request.form["searchKeyword"]

        sessionHandlerController = SessionHandlerController()
        if not sessionHandlerController.doesUserHaveValidSession(userID):
            message = {
                "error": "Session expired"
            }
            return message

        sessionHandlerController.updateLastActiveTime(userID)

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.groupsTable]

        result = collection.find({"meetingTopic": re.compile(searchKeyword, re.IGNORECASE)}).sort(
            [("meetingTime", pymongo.ASCENDING)])
        groups = list(result)
        filteredGroups = list()

        for index, group in enumerate(groups):
            groups[index]["groupID"] = str(group["_id"])
            del groups[index]["_id"]

            groupMembers = group["members"].split("|")

            if userID in groupMembers or not DateTimeHelper.isTimeInGivenBound(
                    groups[index]["meetingTime"], 0):
                continue

            groupMembersList = signupController.getAllUsersForUserIDs(groupMembers)
            members = str()

            for memberIndex, member in enumerate(groupMembersList):
                if memberIndex > 0:
                    members = members + ", "

                members = members + member["firstName"]

            # groups[index]["members"] = members
            groups[index]["members"] = groupMembersList

            filteredGroups.append(groups[index])

        return filteredGroups
