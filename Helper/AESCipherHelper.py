import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES

from Startup import Startup


class AESCipher(object):

    def __init__(self):
        config = Startup.getConfig()
        cipherKey = config["Encryption"]["CipherKey"]
        self.bs = AES.block_size
        self.key = hashlib.sha256(cipherKey.encode()).digest()

    @staticmethod
    def encryptRSA(hash_string):
        sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
        return sha_signature

    def encrypt(self, raw):
        raw = self.pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw.encode()))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self.unPad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def unPad(s):
        return s[:-ord(s[len(s) - 1:])]
