import json


class Startup:
    _config = None

    @staticmethod
    def getConfig():
        if Startup._config is None:
            Startup()
        return Startup._config

    def __init__(self):
        data = dict()
        with open('appsettings/appsettings.json') as f:
            data = json.load(f)
        with open('appsettings/appsettings.Production.json') as f:
            conf = json.load(f)
            data.update(conf)
        Startup._config = data
