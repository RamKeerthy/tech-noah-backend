from threading import Lock

from Helper.ProfanityCheckHelper import ProfanityCheckHelper
from routes.Chat import chatBlueprint
from routes.ForgotPassword import forgotPasswordBlueprint
from routes.Groups import groupBlueprint
from routes.Root import RootBlueprint
from routes.UserLogin import loginBlueprint
from routes.UserSignup import signupBlueprint

import socketio
from flask import session, copy_current_request_context, request, Flask, send_from_directory
from flask_socketio import emit, join_room, leave_room, rooms, close_room, disconnect, SocketIO

asyncMode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins='*')
thread = None
thread_lock = Lock()


@app.route('/<path:path>', methods=['GET'])
def static_proxy(path):
    return send_from_directory('../tech-noah-frontend/', path)


@app.route('/')
def root():
    return send_from_directory('../tech-noah-frontend/', 'index.html')


app.register_blueprint(RootBlueprint, url_prefix="/api/")
app.register_blueprint(loginBlueprint, url_prefix="/api/login")
app.register_blueprint(signupBlueprint, url_prefix="/api/signup")
app.register_blueprint(groupBlueprint, url_prefix="/api/groups")
app.register_blueprint(chatBlueprint, url_prefix="/api/chat")
app.register_blueprint(forgotPasswordBlueprint, url_prefix="/api/forgotPassword")


def backgroundThread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(10)
        count += 1
        socketio.emit('serverGeneratedEvent',
                      {'data': 'Server generated event', 'count': count})


@socketio.event
def myEvent(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('event',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.event
def myBroadcastEvent(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('broadcast',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)


@socketio.on('join')
def join(roomDetail):
    join_room(roomDetail['name'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('joinRoom',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('message')
def handleMessage(message):
    groupID = message['groupID']
    messageContent = message['messageContent']

    print('received message: ', messageContent)
    # send(message, json=True)
    if ProfanityCheckHelper.isProfanity(messageContent):
        message["messageContent"] = ProfanityCheckHelper.censorProfanity(message["messageContent"])

    emit('message', message, room=groupID)


@socketio.event
def leave(message):
    leave_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('leave',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('closeRoom')
def onCloseRoom(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('onCloseRoom', {'data': 'Room ' + message['room'] + ' is closing.',
                         'count': session['receive_count']},
         to=message['room'])
    close_room(message['room'])


@socketio.event
def myRoomEvent(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('myRoomEvent',
         {'data': message['data'], 'count': session['receive_count']},
         to=message['room'])


@socketio.event
def disconnectRequest():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    session['receive_count'] = session.get('receive_count', 0) + 1
    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit('disconnectRequest',
         {'data': 'Disconnected!', 'count': session['receive_count']},
         callback=can_disconnect)


@socketio.event
def myPing():
    emit('my_pong')


@socketio.event
def connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(backgroundThread)
    emit('connect', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect')
def testDisconnect():
    print('Client disconnected', request.sid)
