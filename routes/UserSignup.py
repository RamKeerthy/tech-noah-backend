from flask import Blueprint, request

from Controllers.SignupController import SignupController
from Helper.MongoDBHelper import MongoDBHelper

signupBlueprint = Blueprint('signup', __name__, )
mongoDBHelper = MongoDBHelper()


@signupBlueprint.route('/')
def index():
    return "Signup working"


@signupBlueprint.route('/newUser', methods=['POST'])
def newUser():
    if request.method == 'POST':
        message = None
        try:
            signupController = SignupController()
            message = signupController.signupUser(request)
            message = {"id": message}
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@signupBlueprint.route('/isEmailAvailable', methods=['POST'])
def isEmailAvailable():
    if request.method == 'POST':
        message = None
        try:
            signupController = SignupController()
            message = signupController.isEmailAvailable(request)
            message = {"isEmailAvailable": message}
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@signupBlueprint.route('/validateEmail', methods=['POST'])
def validateEmail():
    if request.method == 'POST':
        message = None
        try:
            signupController = SignupController()
            message = signupController.validateEmail(request)
            message = {"isEmailValid": message}
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message
