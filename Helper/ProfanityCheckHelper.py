from better_profanity import profanity


class ProfanityCheckHelper:
    @staticmethod
    def isProfanity(text):
        return profanity.contains_profanity(text)

    @staticmethod
    def censorProfanity(text):
        return profanity.censor(text)
