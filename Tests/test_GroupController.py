from unittest import TestCase


class TestGroupController(TestCase):
    def test_create_group(self):
        self.assertEqual(1, 1, "Pass")

    def test_update_members(self):
        self.assertEqual(1, 1, "Pass")

    def test_get_group_details(self):
        self.assertEqual(1, 1, "Pass")

    def test_get_all_groups_for_user(self):
        self.assertEqual(1, 1, "Pass")

    def test_get_all_groups(self):
        self.assertEqual(1, 1, "Pass")

    def test_get_all_users_for_group_id(self):
        self.assertEqual(1, 1, "Pass")

    def test_get_groups_for_keyword(self):
        self.assertEqual(1, 1, "Pass")
