from flask import Blueprint, request

from Controllers.LoginController import LoginController

loginBlueprint = Blueprint('login', __name__, )


@loginBlueprint.route('/')
def index():
    return 'login working'


@loginBlueprint.route('/userLogin', methods=['POST'])
def userLogin():
    if request.method == 'POST':
        message = None
        try:
            loginController = LoginController()
            message = loginController.userLogin(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message
