from datetime import datetime, timezone, timedelta
from dateutil import tz


class DateTimeHelper:
    @staticmethod
    def getCurrentUTCDateTime():
        return datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")

    @staticmethod
    def localDateTimeToUTC(localDateTime):
        localDateTimeObject = datetime.strptime(localDateTime, "%Y-%m-%d %H:%M:%S")
        timestamp = localDateTimeObject.timestamp()
        UTCDateTime = datetime.fromtimestamp(timestamp, tz=timezone.utc)
        UTCDateTime = UTCDateTime.strftime("%Y-%m-%d %H:%M:%S")
        return UTCDateTime

    @staticmethod
    def UTCtoLocalDateTime(UTCDateTime):
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()

        UTCDateTimeObject = datetime.strptime(UTCDateTime, "%Y-%m-%d %H:%M:%S")

        utc = UTCDateTimeObject.replace(tzinfo=from_zone)

        # Convert time zone
        localDateTime = utc.astimezone(to_zone)
        localDateTime = localDateTime.strftime("%Y-%m-%d %H:%M:%S")

        return localDateTime

    @staticmethod
    def isTimeInGivenBound(dateTime, minutesBound=120):
        dateTimeObject = datetime.strptime(dateTime, "%Y-%m-%d %H:%M:%S")
        currentTime = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
        currentTime = datetime.strptime(currentTime, "%Y-%m-%d %H:%M:%S")

        addedDateTime = dateTimeObject + timedelta(minutes=minutesBound)
        addedDateTime = addedDateTime.strftime("%Y-%m-%d %H:%M:%S")
        addedDateTime = datetime.strptime(addedDateTime, "%Y-%m-%d %H:%M:%S")

        dateTimeDifference = (addedDateTime - currentTime).total_seconds() / 60.0

        if dateTimeDifference > 0:
            return True
        else:
            return False

    @staticmethod
    def getDateTimeFromString(dateTimeString, dateTimeFormat="%Y-%m-%d %H:%M:%S"):
        return datetime.strptime(dateTimeString, dateTimeFormat)
