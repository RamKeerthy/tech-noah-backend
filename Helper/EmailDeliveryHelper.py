import base64
from calendar import Calendar

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Attachment
from datetime import datetime, timedelta
import pytz
from icalendar import Calendar, Event, vCalAddress, vText

from Helper.DateTimeHelper import DateTimeHelper
from Helper.MongoDBHelper import MongoDBHelper
from Startup import Startup


class EmailDeliveryHelper:
    sendGridAPIKey: str
    mongoClient = None
    database = None
    emailContentTable = None

    def __init__(self):
        config = Startup.getConfig()
        self.sendGridAPIKey = config["sendGridAPIKey"]
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.emailContentTable = MongoDBHelper.tables["mail_contents"]

    def sendActivationCodeEmail(self, firstName, lastName, email, activationCode):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.emailContentTable]

        myQuery = {"emailType": "emailActivation"}
        requiredFields = {
            "emailType": 1,
            "emailContent": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        content = str(result["emailContent"])

        content = content.replace("--firstName--", firstName)
        content = content.replace("--lastName--", lastName)
        content = content.replace("--activationCode--", str(activationCode))

        message = Mail(
            from_email='ucs13434@rmd.ac.in',
            to_emails=email,
            subject='Tech-Noah email verification',
            html_content=content,
        )

        sg = SendGridAPIClient(self.sendGridAPIKey)
        response = sg.send(message)
        return response

    def sendForgotPasswordEmail(self, firstName, lastName, email, activationCode):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.emailContentTable]

        myQuery = {"emailType": "resetPassword"}
        requiredFields = {
            "emailType": 1,
            "emailContent": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        content = str(result["emailContent"])

        content = content.replace("--firstName--", firstName)
        content = content.replace("--lastName--", lastName)
        content = content.replace("--activationCode--", str(activationCode))

        message = Mail(
            from_email='ucs13434@rmd.ac.in',
            to_emails=email,
            subject='Tech-Noah password reset',
            html_content=content,
        )

        sg = SendGridAPIClient(self.sendGridAPIKey)
        response = sg.send(message)
        return response

    def sendZoomMeetingLink(self, groupDict, toEmail, firstName, lastName):
        cal = Calendar()
        cal.add('prodid', 'Tech-Noah product')
        cal.add('version', '0.0.1')

        meetingStartTime = pytz.utc.localize(DateTimeHelper.getDateTimeFromString(groupDict["meetingTime"])) #.tzinfo(pytz.utc)
        meetingTimeStamp = meetingStartTime + timedelta(minutes=10)
        meetingEndTime = meetingStartTime + timedelta(minutes=int(groupDict["meetingDuration"]))

        event = Event()
        event.add('summary', groupDict["meetingTopic"] + ' - Study Buddies Zoom Meeting')
        event.add('description', groupDict["zoomMeetingLink"])
        event.add('dtstart', meetingStartTime)
        event.add('dtend', meetingEndTime)
        event.add('dtstamp', meetingTimeStamp)

        organizer = vCalAddress('MAILTO:ucs13434@rmd.ac.in')

        organizer.params['cn'] = vText("Tech-Noah")
        organizer.params['role'] = vText('CHAIR')
        event['organizer'] = organizer
        event['location'] = vText('Zoom Meeting')

        event['uid'] = groupDict["zoomMeetingLink"]
        event.add('priority', 5)

        attendee = vCalAddress(toEmail)
        attendee.params['cn'] = vText(firstName + " " + lastName)
        attendee.params['ROLE'] = vText('REQ-PARTICIPANT')
        event.add('attendee', attendee, encode=0)

        cal.add_component(event)
        iCalContent = cal.to_ical()

        calendarAttachment = Attachment()
        calendarAttachment.file_name = "invite.ics"
        calendarAttachment.file_content = str(base64.b64encode(iCalContent), 'utf-8')
        calendarAttachment.file_type = "text/calendar"

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.emailContentTable]

        myQuery = {"emailType": "zoomMeeting"}
        requiredFields = {
            "emailType": 1,
            "emailContent": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        content = str(result["emailContent"])

        content = content.replace("--firstName--", firstName)
        content = content.replace("--lastName--", lastName)
        content = content.replace("--studyBuddyGroupName--", groupDict["meetingTopic"])
        content = content.replace("--fromDateTime--", str(meetingStartTime))
        content = content.replace("--toDateTime--", str(meetingEndTime))
        content = content.replace("--zoomMeetingLink--", groupDict["zoomMeetingLink"])

        message = Mail(
            from_email='ucs13434@rmd.ac.in',
            to_emails=toEmail,
            subject='Study Buddies Meeting Invitation',
            html_content=content,
        )

        message.attachment = calendarAttachment

        sg = SendGridAPIClient(self.sendGridAPIKey)
        response = sg.send(message)
        return response
