import json
from zoomus import ZoomClient
import pyzoom

from Startup import Startup


class ZoomAPIHelper:
    APIKey = None
    APISecret = None
    client = None
    groupID = None
    pyZoomClient = None

    def __init__(self):
        config = Startup.getConfig()
        self.APIKey = config["ZoomAPICredentials"]["APIKey"]
        self.APISecret = config["ZoomAPICredentials"]["APISecret"]
        self.groupID = config["ZoomAPICredentials"]["groups"]["studyBuddies"]

        self.client = ZoomClient(self.APIKey, self.APISecret)
        self.pyZoomClient = pyzoom.ZoomClient(self.APIKey, self.APISecret)

    def addUserToZoom(self, action, userInfo):
        userAddResponse = self.client.user.create(action=action, user_info=userInfo)
        userAddDict = json.loads(userAddResponse.content)

        return userAddDict

    def addUserToGroup(self, members):
        addMemberToGroup = self.client.group.add_members(groupid=self.groupID, members=members)
        addMemberToGroupContent = json.loads(addMemberToGroup.content)

        return addMemberToGroupContent

    def createMeeting(self, hostEmail, topic, startTime, duration):
        meetingDetails = self.pyZoomClient.meetings.create_meeting(topic=topic,
                                                                   start_time=startTime,
                                                                   duration_min=duration,
                                                                   password='not-secure',
                                                                   timezone='UTC')

        print(meetingDetails)

        return meetingDetails
