from bson import ObjectId

from Controllers.SessionHandlerController import SessionHandlerController
from Helper.AESCipherHelper import AESCipher
from Helper.DateTimeHelper import DateTimeHelper
from Helper.EmailDeliveryHelper import EmailDeliveryHelper
from Helper.MongoDBHelper import MongoDBHelper
from Helper.IDGenerator import IDGenerator
from Models.User import User


class SignupController:
    mongoClient = None
    database = None
    usersTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.usersTable = MongoDBHelper.tables["users"]

    def getAllUsers(self):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        selectFields = {
            "email": 1,
            "firstName": 1,
            "lastName": 1,
            "organizationName": 1,
            "highestDegree": 1,
            "major": 1,
            "tags": 1,
            "ratings": 1,
            "sessionID": 1,
            "_id": True
        }

        result = list(collection.find({}, selectFields))
        return result

    def signupUser(self, request):
        user = User()

        user.firstName = request.form["firstName"]
        user.lastName = request.form["lastName"]
        user.dateOfBirth = request.form["dateOfBirth"]
        user.email = request.form["email"]
        user.password = AESCipher.encryptRSA(request.form["password"])
        user.phone = request.form["phone"]
        user.organizationName = request.form["organizationName"]
        user.highestDegree = request.form["degree"]
        user.major = request.form["major"]
        user.tags = request.form["tags"]
        user.reportCount = 0

        # Generate activation code and send it via email
        user.activationCode = IDGenerator.randomWithNDigits(8)
        # Send the activation code to email
        emailDeliveryHelper = EmailDeliveryHelper()
        response = emailDeliveryHelper.sendActivationCodeEmail(user.firstName, user.lastName, user.email,
                                                               user.activationCode)

        user.dateCreated = DateTimeHelper.getCurrentUTCDateTime()
        user.ratings = 0.0
        user.isVerified = False

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]
        userDict = user.__dict__

        response = collection.insert_one(userDict)

        sessionHandlerController = SessionHandlerController()
        sessionHandlerController.createNewSessionID(str(response.inserted_id))

        # zoomController = ZoomController()
        # zoomController.addUserToZoom(response.inserted_id, user)

        return str(response.inserted_id)

    def isEmailAvailable(self, request):
        email = request.form["email"]

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        result = collection.find({"email": email})
        result = list(result)

        if len(result) > 0:
            return True
        else:
            return False

    def validateEmail(self, request):
        email = request.form["email"]
        password = AESCipher.encryptRSA(request.form["password"])
        activationCode = request.form["activationCode"]

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        result = collection.find({"email": email})
        result = list(result)

        if len(result) > 0:
            result = result[0]
            if result['password'] != password:
                return "Entered password did not match"
            elif result['activationCode'] != int(activationCode):
                return "Entered an invalid activation code"
        else:
            return "Email ID not yet registered"

        # update isVerified on DB
        myQuery = {
            "email": result["email"]
        }

        updatedIsValid = {
            "$set": {
                "isVerified": True
            }
        }
        collection.update_one(myQuery, updatedIsValid)

        return True

    def getUserIDForEmail(self, email):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        myQuery = {
            "email": email
        }

        requiredFields = {
            "_id": True,
            "email": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        return str(result["_id"])

    def getUserInfoForUserID(self, userID, requiredDetail="email"):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        myQuery = {
            "_id": ObjectId(userID)
        }

        requiredFields = {
            "_id": True,
            "email": 1,
            "firstName": 1,
            "lastName": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        return str(result[requiredDetail])

    def getAllUserInfoForUserID(self, userID):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        myQuery = {
            "_id": ObjectId(userID)
        }

        requiredFields = {
            "_id": True,
            "email": 1,
            "firstName": 1,
            "lastName": 1
        }

        result = collection.find_one(myQuery, requiredFields)
        return str(result)

    def getAllUserIdAndEmail(self):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        result = collection.find({})
        return list(result)

    def getAllUsersForUserIDs(self, userList):
        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        objIDs = [ObjectId(userID) for index, userID in enumerate(userList)]

        myQuery = {
            "_id": {
                "$in": objIDs
            }
        }

        requiredFields = {
            "_id": True,
            "email": 1,
            "firstName": 1
        }

        result = collection.find(myQuery, requiredFields)
        result = list(result)

        for index, user in enumerate(result):
            result[index]["_id"] = str(user["_id"])

        return result
