from flask import Blueprint

RootBlueprint = Blueprint('root', __name__, )


@RootBlueprint.route('/')
def index():
    return 'Tech Noah API root path'
