from datetime import datetime

from bson import ObjectId
from dateutil import tz

from Helper.MongoDBHelper import MongoDBHelper
from Helper.ZoomAPIHelper import ZoomAPIHelper


class ZoomController:
    mongoClient = None
    database = None
    usersTable = None
    groupsTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.usersTable = MongoDBHelper.tables["users"]
        self.groupsTable = MongoDBHelper.tables["groups"]

    def addUserToZoom(self, userID, user):
        # add user to zoom
        zoomAPIHelper = ZoomAPIHelper()
        userInfo = {
            "email": user.email,
            "type": 1,
            "first_name": user.firstName,
            "last_name": user.lastName
        }
        userAddDict = zoomAPIHelper.addUserToZoom(action='create', userInfo=userInfo)

        if 'id' not in userAddDict:
            message = {
                "error": "User cannot be added"
            }
            return message

        # update the zoomID on user table
        databaseConnection = self.mongoClient[self.database]
        userCollection = databaseConnection[self.usersTable]

        myQuery = {
            "_id": ObjectId(userID)
        }

        updateGroupMembers = {
            "$set": {
                "zoomID": userAddDict['id']
            }
        }
        userCollection.update_one(myQuery, updateGroupMembers)

        # add user to zoom group
        members = [
            {
                "id": userAddDict['id'],
                "email": user.email
            }
        ]
        addMemberToGroupContent = zoomAPIHelper.addUserToGroup(members)

        message = {
            "zoomUserID": userAddDict['id']
        }

        return message

    def createZoomMeeting(self, hostEmail, topic, startTime, duration):
        from_zone = tz.tzutc()
        UTCDateTimeObject = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")

        utc = UTCDateTimeObject.replace(tzinfo=from_zone)
        utcIsoStartTime = utc.isoformat()

        zoomHelper = ZoomAPIHelper()
        meetingDetails = zoomHelper.createMeeting(hostEmail, topic, utcIsoStartTime, duration)

        return meetingDetails
