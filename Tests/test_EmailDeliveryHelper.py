from unittest import TestCase


class TestEmailDeliveryHelper(TestCase):
    def test_send_activation_code_email(self):
        self.assertEqual(1, 1, "Pass")

    def test_send_forgot_password_email(self):
        self.assertEqual(1, 1, "Pass")

    def test_send_zoom_meeting_link(self):
        self.assertEqual(1, 1, "Pass")
