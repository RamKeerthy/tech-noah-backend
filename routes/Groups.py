from flask import Blueprint, request

from Controllers.GroupController import GroupController

groupBlueprint = Blueprint('groups', __name__, )


@groupBlueprint.route('/')
def index():
    return 'groups working'


@groupBlueprint.route('/createGroup', methods=['POST'])
def createGroup():
    if request.method == 'POST':
        message = None
        try:
            groupController = GroupController()
            message = groupController.createGroup(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@groupBlueprint.route('/updateMembers', methods=['POST'])
def updateMembers():
    if request.method == 'POST':
        message = None
        try:
            groupController = GroupController()
            message = groupController.updateMembers(request)
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@groupBlueprint.route('/getAllGroupsForUser', methods=['POST'])
def getAllGroupsForUser():
    if request.method == 'POST':
        message = None
        try:
            groupController = GroupController()
            message = groupController.getAllGroupsForUser(request)
            message = {
                "groups": message
            }
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message


@groupBlueprint.route('/getAllGroups', methods=['POST'])
def getAllGroups():
    if request.method == 'POST':
        message = None
        try:
            groupController = GroupController()
            message = groupController.getAllGroups(request)
            message = {
                "groups": message
            }
        except Exception as e:
            if hasattr(e, 'message'):
                message = {"error": str(e.message)}
            else:
                message = {"error": str(e)}
        return message
