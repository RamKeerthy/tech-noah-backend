class User:
    firstName: str
    lastName: str
    dateOfBirth: str
    email: str
    password: str
    phone: str
    organizationName: str
    highestDegree: str
    major: str
    tags: str

    isVerified: bool
    activationCode: str
    dateCreated: str
    ratings: float
    zoomID: str
    reportCount: str
