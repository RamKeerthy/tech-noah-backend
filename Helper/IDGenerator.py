from random import randint
import uuid


class IDGenerator:
    @staticmethod
    def randomWithNDigits(n):
        range_start = 10 ** (n - 1)
        range_end = (10 ** n) - 1
        return randint(range_start, range_end)

    @staticmethod
    def generateNewSessionID():
        return str(uuid.uuid4())
