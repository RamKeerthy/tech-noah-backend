from Controllers.SessionHandlerController import SessionHandlerController
from Helper.AESCipherHelper import AESCipher
from Helper.MongoDBHelper import MongoDBHelper
from bson.json_util import dumps


class LoginController:
    mongoClient = None
    database = None
    usersTable = None

    def __init__(self):
        self.mongoClient = MongoDBHelper.mongoClient
        self.database = MongoDBHelper.getDatabaseName()
        self.usersTable = MongoDBHelper.tables["users"]

    def userLogin(self, request):
        email = request.form["email"]
        password = AESCipher.encryptRSA(request.form["password"])

        databaseConnection = self.mongoClient[self.database]
        collection = databaseConnection[self.usersTable]

        query = {
            "email": email,
            "password": password
        }

        selectFields = {
            "email": 1,
            "firstName": 1,
            "lastName": 1,
            "organizationName": 1,
            "highestDegree": 1,
            "major": 1,
            "tags": 1,
            "ratings": 1,
            "sessionID": 1,
            "_id": True
        }

        result = collection.find(query, selectFields)
        result = list(result)

        if len(result) > 0:
            user = result[0]

            sessionHandlerController = SessionHandlerController()
            sessionID = sessionHandlerController.createSessionID(str(user["_id"]))

            sessionID = {
                "sessionID": sessionID
            }

            user.update(sessionID)
            return dumps(user)
        else:
            result = {
                "error": "Invalid Email ID or Password"
            }
            return result
